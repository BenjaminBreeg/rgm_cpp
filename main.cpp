#include <iostream>
#include"libusb-1.0/libusb.h"
#include"rtl-sdr.h"
#include<ctime>
#include<malloc.h>
#include<memory>
#include<string.h>
#include<stdlib.h>
#include<bitset>

using namespace std;
struct sPocket
{
    bool isFall;
    uint16_t time;
    uint8_t *buffer;
};
/*algorythmes*/
float integrate(float *arr, int lenght)
{
    float summ=0;
    for(int i=0;i<lenght;i++)
    {
        summ+=arr[i];
    }
    return summ;
}
float DCBlock(float *arr, int lenght)
{
    float DC=integrate(arr,lenght)/lenght;
    if(DC==0)
    {
        return DC;
    }
    for(int i=0;i<lenght;i++)
    {
        if(DC>0)
        {
            arr[i]-=DC;
        }
        else
        {
            arr[i]+=DC;
        }
    }
    return DC;
}
void rgmByAmplitude()
{
    int lenght=1024;
    float time=10;
    //float *signal=generateSignal(lenght);
    //displaySignal(signal,lenght);
    //float DC=DCBlock(signal, lenght);
    //displaySignal(signal,lenght);
    //cout<<DC;
}
/*algorythmes*/
int last_buf=0;
unsigned short int *signal_buf;
float decode(unsigned char *buf)
{
    last_buf=(255*last_buf+int(*buf))>>8;
    return last_buf;
}
void rtlsdr_callback(unsigned char *buf, uint32_t len, void *ctx)
{
    //cout<<"some callback???"<<endl;
    cout<<decode(buf)<<endl;
    //cout<<decode(buf)<<endl;
}
class cRtlsdr
{
    rtlsdr_dev_t *dev;
    int deviceIndex;
    //sdr params block
    bool testMode;
    int gain;
    int32_t centerFreq;
    int32_t sampleRate;
    float ppm;
    int bufSize;
    uint8_t *buffer;
    int nRead;
    double *resBuf;
    unsigned int signal_buf_size;
public:
    cRtlsdr()
    {
        dev=NULL;
        testMode=false;
        deviceIndex=0;
        gain=0;
        centerFreq=300000000;
        sampleRate=2560000;
        ppm=0.00001;
        bufSize=512;
    `   signal_buf_size=32768;
    }
    bool init()
    {
        int result=1;
        result=rtlsdr_open(&dev,deviceIndex);//open device
        if(result<0)
        {
            return false;
        }
        result=rtlsdr_set_testmode(dev,int(testMode));//setting rest mode to false
        if(result<0)
        {
            return false;
        }
        result=rtlsdr_reset_buffer(dev);//reset buffer before work
        if(result<0)
        {
            return false;
        }
        rtlsdr_set_sample_rate(dev,sampleRate);
        rtlsdr_set_center_freq(dev,centerFreq);
        rtlsdr_set_agc_mode(dev, 1);
        //buffer=malloc(bufSize*sizeof(uint8_t));
        buffer=new uint8_t(sizeof(uint8_t)*bufSize);
        resBuf=new double(sizeof(double)*bufSize);
        return true;
    }
    sPocket getPocketAsync()
    {
        int result=1;
        sPocket pack;
        cout<<"tst";
        last_buf=128;
        signal_buf=new unsigned short int(sizeof(unsigned short int)*signal_buf_size);
        result=rtlsdr_read_async(dev,rtlsdr_callback,NULL,0, 0);
        cout<<"tts";
        if(result<0)
        {
            pack.isFall=true;
            pack.buffer=NULL;
            return pack;
        }
        pack.isFall=false;
        pack.buffer=buffer;
        return pack;
    }
    bool resetArray(float *arr, int size)
    {
        for(int i=0;i<size;i++)
        {
            arr[i]=0;
        }
        return true;
    }
    bool resetArray(uint8_t *arr, int size)
    {
        for(int i=0;i<size;i++)
        {
            arr[i]=0;
        }
        return true;
    }
    void readToBuffer()
    {
        nRead=0;
        rtlsdr_reset_buffer(dev);
        rtlsdr_read_sync(dev,buffer,bufSize,&nRead);
        cout<<nRead<<endl;
    }
    bool closeDevice()
    {
        if(rtlsdr_close(dev)<0);
        {
            return false;
        }
        return true;

    }
};

int main()
{
    cout<<".:*** welcome to rtl-sdr-raspi ***:."<<endl;
    cRtlsdr *sdr=new cRtlsdr();
    sdr->init();
    sdr->getPocketAsync();
    //rgmByAmplitude();
    cin.get();
    cin.get();
    return 0;
}
